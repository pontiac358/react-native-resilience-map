Pod::Spec.new do |s|
  s.name             = 'react-native-resilience-map'
  s.version          = '1.0.0'
  s.summary          = 'YandexMap component'

  s.description      = <<-DESC
    YandexMap component for react native
                       DESC

  s.homepage         = 'https://github.com/arturkh/react-native-resilience-map'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Resilience' => 'rresilience8@gmail.com' }
  s.source           = { :git => 'https://github.com/arturkh/react-native-resilience-map.git', :tag => s.version.to_s }

  s.ios.deployment_target = '10.0'
  s.source_files = 'ios/*.{h,m}'
  s.dependency 'React'
  s.dependency 'YandexMapKit', '3.4'
end
