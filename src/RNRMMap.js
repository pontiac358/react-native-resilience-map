import PropTypes from 'prop-types';
import React, { Component } from 'react';
import {requireNativeComponent, NativeModules } from 'react-native';

const RNRMMapKit = NativeModules.RNRMMapViewKit;
export default class RNRMMap extends Component {
	_onCameraPositionChanged = (event) => {
		this.props.onCameraPositionChanged(event.nativeEvent);
  	}
	render() {
		return (
	    	<RNRMMapView 
	    		style={{ flex: 1 }}
	    		zoom={this.props.zoom}
	    		onCameraPositionChanged={this._onCameraPositionChanged}
	    	/>
		);
	}

	static setApiKey(apiKey) {
		RNRMMapKit.setApiKey(apiKey);
	};
}

RNRMMap.propTypes = {
	zoom: PropTypes.number,
	onCameraPositionChanged: PropTypes.func
};

const RNRMMapView = requireNativeComponent('RNRMMapView', RNRMMap);
module.exports = RNRMMap;