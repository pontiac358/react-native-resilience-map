
# react-native-resilience-map

## Установка

`$ npm install react-native-resilience-map@git@bitbucket.org:delivery05/react-native-resilience-map.git --save`


#### iOS

1. Добавить в Podfile `pod 'react-native-resilience-map', :path => '../node_modules/react-native-resilience-map'`
2. Выполнить команду `pod install`

#### Android



## Usage
```javascript
import RNRMMap from 'react-native-resilience-map';

RNRMMap.setApiKey("****");

<RNRMMap zoom={float} onCameraPositionChanged={__function__}/>
  
onCameraPositionChanged event:
{ 
	latitude: 0.0,
	longitude: 0.0,
	finished: true
}
