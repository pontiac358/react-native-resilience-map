//
//  RNRMMapViewKit.m
//
//  Created by Resilience on 18.12.2019.
//
#import <React/RCTBridge.h>
#import <YandexMapKit/YMKMapKitFactory.h>
#import "RNRMMapViewKit.h"

@implementation RNRMMapViewKit
- (dispatch_queue_t)methodQueue {
    return dispatch_get_main_queue();
}

RCT_EXPORT_MODULE()
RCT_EXPORT_METHOD(setApiKey:(NSString *)apiKey)
{
  [YMKMapKit setApiKey:apiKey];
}
@end
