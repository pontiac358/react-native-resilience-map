//
//  RNRMMapViewManager.h
//
//  Created by Resilience on 12.12.2019.
//
#import <YandexMapKit/YMKMapView.h>
#import <CoreLocation/CoreLocation.h>
#import "RCTViewManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface RNRMMapViewManager : RCTViewManager <CLLocationManagerDelegate>

@end

NS_ASSUME_NONNULL_END
