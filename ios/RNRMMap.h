#import <YandexMapKit/YMKMapKit.h>
#import <React-Core/React/RCTComponent.h>
#import <React-Core/React/RCTView.h>

@interface RNRMMap : RCTView <YMKMapCameraListener>
@property (nonatomic, assign) float zoom;
@property (nonatomic, copy) RCTDirectEventBlock onCameraPositionChanged;
- (void)setCenterLatitude:(double)latitude andLongitude:(double)longitude withZoom:(float)zoom;
@end
  
