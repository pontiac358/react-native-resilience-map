//
//  RNRMMapViewManager.m
//
//  Created by Resilience on 12.12.2019.
//

#import <React/RCTBridge.h>
#import <CoreLocation/CoreLocation.h>
#import "RNRMMapViewManager.h"
#import "RNRMMap.h"

@implementation RNRMMapViewManager {
    CLLocationManager *locationManager;
    RNRMMap *mapView;
}
RCT_EXPORT_MODULE();
RCT_EXPORT_VIEW_PROPERTY(zoom, float)
RCT_EXPORT_VIEW_PROPERTY(showUserLocation, false)
RCT_EXPORT_VIEW_PROPERTY(onCameraPositionChanged, RCTDirectEventBlock)

- (NSArray *) customDirectEventTypes {
  return @[
      @"onCameraPositionChanged"
  ];
}

- (UIView *)view {
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager requestWhenInUseAuthorization];
    [locationManager startUpdatingLocation];
    
    mapView = [[RNRMMap alloc] init];
    return mapView;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    [locationManager stopUpdatingLocation];
    CLLocationCoordinate2D coordinate = locations.lastObject.coordinate;
    [mapView setCenterLatitude:coordinate.latitude andLongitude:coordinate.longitude withZoom:mapView.zoom];
}
@end
