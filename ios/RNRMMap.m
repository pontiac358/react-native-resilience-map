#import <React/UIView+React.h>
#import "RNRMMap.h"
#import <YandexMapKit/YMKMapView.h>

@implementation RNRMMap {
    YMKMapView *mapView;
    YMKMap *map;
    
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        mapView = [[YMKMapView alloc] init];
        mapView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self addSubview:mapView];
        map = mapView.mapWindow.map;
        [map addCameraListenerWithCameraListener:self];
    }

  return self;
}

- (void)setCenterLatitude:(double)latitude andLongitude:(double)longitude withZoom:(float)zoom {
    YMKPoint *point = [YMKPoint pointWithLatitude:latitude longitude:longitude];
    YMKCameraPosition *position = [YMKCameraPosition cameraPositionWithTarget:point zoom:zoom azimuth:0 tilt:0];
    [map moveWithCameraPosition:position];
}

- (void)onCameraPositionChangedWithMap:(YMKMap *)map cameraPosition:(YMKCameraPosition *)cameraPosition cameraUpdateSource:(YMKCameraUpdateSource)cameraUpdateSource finished:(BOOL)finished {
    if (_onCameraPositionChanged) {
        _onCameraPositionChanged(@{@"latitude": @(cameraPosition.target.latitude), @"longitude": @(cameraPosition.target.longitude), @"finished": @(finished)});
    }
}

@end
  
